package json;

import java.io.FileWriter;
import java.io.IOException;
import org.json.simple.JSONObject;

import java.util.Random;

public class JsonGenerator {
	@SuppressWarnings("unchecked")
	 public static void main( String[] args )
	    {
	        
			JSONObject obj = new JSONObject();

	        int noFields = 250;
	        int min = 5;
	        int max=30;   
	        int randomNum;
	        Random rand = new Random();
	        
	        int noFiles = 300;
	        
	        String fname;
	        String baseName = "300file250field/data";
	        
	        // Generate duplicate values for 5 fields: 5 duplicate values for field 1, 10 duplicate values for field 2,...
	        int[] duplicateNo= {1,5,10,15,20};
	        int[] duplicateCount = {0,0,0,0,0};
	        String[] duplicateVals = {"PQR5o","hdApEnq9","oSmQWfp","y2b-WfV","Zh7ZgUPkHe"};
	        boolean[] duplicate = {true,true,true,true,true}; //true means continuing input duplicate values
	        int l = duplicateNo.length;
	        int maxDuplicate = duplicateNo[l-1]; //the maximum number of duplicate values
	        
	        for(int k=0; k< noFiles; k++) {
	        	if(k<=maxDuplicate) {
		        	for(int j=0; j< l; j++) { //count the number of usage of duplicate values
		        		if(duplicate[j]==true && duplicateCount[j]>=duplicateNo[j]) // stop to input duplicate values
		        		{
		        			duplicate[j] = false;
		        		}
		        		duplicateCount[j]++;
		        	}
	        	}
				for (int i = 0; i < noFields ; i=i+1) {
					String key = "field" + i;
					if(i<l && duplicate[i]==true) {
						String value = duplicateVals[i];
						obj.put(key,value);
					}
					else {
						randomNum = rand.nextInt((max - min) + 1) + min;
						String value = RandomString.generate(randomNum);
						obj.put(key,value);
					}
				}
				 //Write JSON file
				fname = baseName + k + ".json";
		        try (FileWriter file = new FileWriter(fname)) {
		 
		            file.write(obj.toJSONString());
		            file.flush();
		 
		        } catch (IOException e) {
		            e.printStackTrace();
		        }
	        }    
	    }
}
